var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var sockethose = require('socket.io-stream');
var ssh = require('ssh2').Client;

var config = require('./config.json');

var in_use = [];

app.use('/bower_components',  express.static(__dirname + '/bower_components'));
app.use('/',  express.static(__dirname + '/public'));

app.get('/socket.io-stream.js', function (req, res) {
	res.sendFile(__dirname + '/node_modules/socket.io-stream/socket.io-stream.js');
});
app.get('/config.js', function (req, res) {
	res.sendFile(__dirname + '/client-config.js');
});

io.on('connection', function (socket) {
	socket.on('auth', function (data) {
		
		if(in_use.indexOf(socket) != -1) return;
		in_use.push(socket);
		
		var cols = parseInt(data.cols) || 80,
			rows = parseInt(data.rows) || 24;
		
		var conn = new ssh();
		
		conn
			.on('ready',	init_stream(socket, conn, rows, cols))
			.on('error',	error(socket, conn))
			.on('end',		end(socket, conn))
		;
		
		conn.on('keyboard-interactive', function(name, instructions, instructionsLang, prompts, finish) {
			finish([data.password || '']);
		});
		
		conn.connect({
			host: config.target.host,
			port: config.target.port,
			username: data.username || '',
			password: data.password || '',
			tryKeyboard: config.target.keyboard
		});
		
		socket.on('disconnect', function() {
			conn.end();
		});
		
	});
});

function init_stream(socket, conn, rows, cols) { return function() {
	socket.emit('conn');
	
	// receives a bidirectional pipe from the client see index.html
	// for the client-side
	var sock = sockethose(socket);
	sock.on('data', function(stream) {
		conn.shell(function(err, shell) {
			if (err) throw err;
			
			shell.setWindow(rows, cols);
			shell.on('close', function() { conn.end(); });
			shell.stdout.pipe(stream).pipe(shell.stdin);
			
			conn.on('end', function() {
				stream.unpipe();
				shell.stdout.unpipe();
				sock.removeAllListeners('data');
			});
			
			socket.on('resize', function (data) {
				var cols = parseInt(data.cols),
					rows = parseInt(data.rows);
				
				if(isNaN(cols) || isNaN(rows)) return;
				
				shell.setWindow(rows, cols);
			});
		});
	});
}}

function error(socket, conn) { return function(err) {
	if(err.level == 'client-authentication') {
		socket.emit('bad-auth');
	}
}}

function end(socket, conn) { return function(err) {
	socket.emit('discon');
	
	var index = in_use.indexOf(socket);
	if (index > -1) {
		in_use.splice(index, 1);
	}
}}

console.log('App listening to http://' + config.listen + ':' + config.port);
server.listen(config.port, config.listen);
