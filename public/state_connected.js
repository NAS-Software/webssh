
function state_connected() {
	term.clear();
	term_reset();
	
	var hose = sockethose(socket);
	var stream = sockethose.createStream();
	hose.emit('data', stream);
	
	stream.on('data', function(buf) {
		term.write(buf.toString());
	});
	
	term.on('data', function(data) {
		stream.write(data);
	});
	
	term.on('resize', function (size) {
		socket.emit('resize', { cols: size.cols, rows: size.rows });
	});
}
