
function state_login() {
	term.clear();
	login_cycle();
	
	socket.on('bad-auth', function() {
		term.write("Bad Login.\r\n");
		login_cycle();
	});
	
	socket.on('conn', function() {
		term.write("\r\n");
		state_connected();
	});
};

function login_cycle() {
	getInput("Username: ", true, function(username) {
	getInput("Password: ", false, function(password) {
		term.write('Logging in...   ');
		auth(username, password);
});});};

function auth(username, password) {
	socket.emit('auth', { cols: termSize.cols, rows: termSize.rows, username: username, password: password });
};

function getInput(prompt, echo, callback) {
	term_reset();
	
	var input = "";
	
	term.write(prompt);
	
	term.on('key', function (key, ev) {
		if (ev.keyCode == 8 && term.x > prompt.length) { // Do not delete the prompt
			if(echo) term.write('\b \b');
			input = input.substring(0, input.length - 1);
		}
	});
	
	term.on('data', function(data) {
		for(char of data) {
			var keycode = char.charCodeAt();
			var printable = keycode >= 32 && keycode <= 126;
			
			if(keycode == 13 && input.length != 0) { // enter
				term.write('\r\n');
				callback(input);
				return;
			} else if(printable && input.length <= 32) { // any text
				if(echo) term.write(char);
				input += char;
			}
		}
	});
}