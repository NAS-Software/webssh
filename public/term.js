var charSize = {};
var termSize = {};

function setup_term() {
	var terminalContainer = document.getElementById('terminal-container');
	
	// Automatically set the size of the terminal based on it's container's size. Opposite of applySize.
	var resize = function() {
		var initialGeometry = term.proposeGeometry();
		
		termSize.cols = initialGeometry.cols;
		termSize.rows = initialGeometry.rows;
		
		charSize.charWidth = Math.ceil(term.element.offsetWidth / termSize.cols);
		charSize.charHeight = Math.ceil(term.element.offsetHeight / termSize.rows);
		
		term.resize(termSize.cols, termSize.rows);
	}
	
	// Clean terminal
	while (terminalContainer.children.length) {
		terminalContainer.removeChild(terminalContainer.children[0]);
	}
	
	term = new Terminal({
		cursorBlink: true
	});
	
	term.open(terminalContainer);
	
	resize();
}
