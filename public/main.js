
var sockethose = ss; // ss isn't that nice to work with. Call it sockethost.
var term;

// connect to the websocket
var socket = io.connect(window.location.protocol + '//' + window.location.host, config.socket_opts);

function term_reset() {
	term.removeAllListeners('key');
	term.removeAllListeners('paste');
	term.removeAllListeners('data');
}

window.addEventListener("load", function() {
	setup_term();
	state_login();
});
